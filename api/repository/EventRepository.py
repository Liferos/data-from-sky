from api import db
from api.entity.Event import Event


def save_event(event):
    try:
        db.session.add(event)
        db.session.commit()
        return 'Event:' + event.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Event: ' + event.__repr__() + ' not saved'


def get_event(event_id):
    try:
        return Event.query.get(event_id)
    except:
        return 'Error during get event with id:' + str(event_id)


def get_event_list():
    try:
        return Event.query.all()
    except:
        return 'Error during get event list'


def delete_event(event_id):
    try:
        Event.query.delete(event_id)
        db.session.commit()
        return 'Event: ' + str(event_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Event: ' + str(event_id) + ' not deleted'
