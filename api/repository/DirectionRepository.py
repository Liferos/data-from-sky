from api import db
from api.entity.Direction import Direction


def save_direction(direction_obj):
    try:
        db.session.add(direction_obj)
        db.session.commit()
        return 'Direction: ' + direction_obj.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Direction: ' + direction_obj.__repr__() + ' not saved'


def get_direction(direction_id):
    try:
        return Direction.query.get(direction_id)
    except:
        return 'Error during get direction with id:' + str(direction_id)


def get_direction_list():
    try:
        return Direction.query.all()
    except:
        return 'Error during get direction list'


def delete_direction(direction_id):
    try:
        Direction.query.delete(direction_id)
        db.session.commit()
        return 'Direction: ' + str(direction_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Direction: ' + str(direction_id) + ' not deleted'
