from api import db
from api.entity.Statistic import Statistic


def save_stat(stat_obj):
    try:
        db.session.add(stat_obj)
        db.session.commit()
        return 'Statistic: ' + stat_obj.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Statistic: ' + stat_obj.__repr__() + ' not saved'


def get_stat(stat_id):
    try:
        return Statistic.query.get(stat_id)
    except:
        return 'Error during get statistic with id:' + str(stat_id)


def get_stat_list():
    try:
        return Statistic.query.all()
    except:
        return 'Error during get statistic list'


def delete_stat(stat_id):
    try:
        Statistic.query.delete(stat_id)
        db.session.commit()
        return 'Statistic: ' + str(stat_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Statistic: ' + str(stat_id) + ' not deleted'

def refresh_stat(statistic):
    try:
        return db.session.refresh(statistic)
    except:
        return 'Error during refresh statistic object'
