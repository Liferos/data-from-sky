from api import db
from api.entity.Profile import Profile


def save_profile(prof_obj):
    try:
        db.session.add(prof_obj)
        db.session.commit()
        return 'Profile: ' + prof_obj.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Profile: ' + prof_obj.__repr__() + ' not saved'


def get_profile(prof_id):
    try:
        return Profile.query.get(prof_id)
    except:
        return 'Error during get profile with id:' + str(prof_id)


def get_profile_list():
    try:
        return Profile.query.all()
    except:
        return 'Error during get profile list'


def delete_profile(prof_id):
    try:
        Profile.query.delete(prof_id)
        db.session.commit()
        return 'Profile: ' + str(prof_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Profile: ' + str(prof_id) + ' not deleted'
