from api import db
from api.entity.Gate import Gate


def save_gate(gate):
    try:
        db.session.add(gate)
        db.session.commit()
        return 'Gate: ' + gate.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Gate: ' + gate.__repr__() + ' not saved'


def get_gate(gate_id):
    try:
        return Gate.query.get(gate_id)
    except:
        return 'Error during get gate with id:' + str(gate_id)


def get_gate_list():
    try:
        return Gate.query.all()
    except:
        return 'Error during get gate list'


def delete_gate(gate_id):
    try:
        Gate.query.delete(gate_id)
        db.session.commit()
        return 'Gate: ' + str(gate_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Gate: ' + str(gate_id) + ' not deleted'
