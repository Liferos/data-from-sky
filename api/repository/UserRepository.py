from api import db
from api.entity.User import User


def save_user(user):
    try:
        db.session.add(user)
        db.session.commit()
        return 'User: ' + user.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'User: ' + user.__repr__() + ' not saved'


def get_user(user_id):
    try:
        return User.query.get(user_id)
    except:
        return 'Error during get user with id:' + str(user_id)


def get_user_list():
    try:
        return User.query.all()
    except:
        return 'Error during get user list'


def delete_user(user_id):
    try:
        User.query.delete(user_id)
        db.session.commit()
        return 'User: ' + str(user_id) + ' deleted'
    except:
        db.session.rollback()
        return 'User: ' + str(user_id) + ' not deleted'
