from api import db
from api.entity.Cam import Cam


def save_cam(cam_obj):
    try:
        db.session.add(cam_obj)
        db.session.commit()
        return 'Cam: ' + cam_obj.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Cam: ' + cam_obj.__repr__() + ' not saved'

def get_cam(cam_id):
    try:
        return Cam.query.get(cam_id)
    except:
        return 'Error during get cam with id:' + str(cam_id)


def get_cam_list():
    print(Cam.query.all())
    try:
        return Cam.query.all()
    except:
        return 'Error during get cam list'


def delete_cam(cam_id):
    try:
        Cam.query.delete(cam_id)
        db.session.commit()
        return 'Cam: ' + str(cam_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Cam: ' + str(cam_id) + ' not deleted'
