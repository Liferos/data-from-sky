from api import db
from api.entity.Object import Object


def save_object(object_):
    try:
        db.session.add(object_)
        db.session.commit()
        return 'Object: ' + object_.__repr__() + ' saved'
    except:
        db.session.rollback()
        return 'Object: ' + object_.__repr__() + ' not saved'


def get_object(object_id):
    try:
        return Object.query.get(object_id)
    except:
        return 'Error during get object with id:' + str(object_id)


def get_object_list():
    try:
        return Object.query.all()
    except:
        return 'Error during get object list'


def delete_object(object_id):
    try:
        Object.query.delete(object_id)
        db.session.commit()
        return 'Object: ' + str(object_id) + ' deleted'
    except:
        db.session.rollback()
        return 'Object: ' + str(object_id) + ' not deleted'
