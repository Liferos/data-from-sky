from api import db


class Object(db.Model):
    __tablename__ = 'object'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)

    def get_value(self):
        return self.value

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return '{' + str(self.id) + ', ' + str(self.value) + '}'
