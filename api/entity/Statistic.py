from api import db


class Statistic(db.Model):
    __tablename__ = 'statistic'

    id = db.Column(db.Integer, primary_key=True)
    gate_id = db.Column(db.Integer, db.ForeignKey('gate.id'))
    events = db.relationship('Event', backref='statistic', lazy=True)
    timestamp = db.Column(db.TIMESTAMP)

    def get_timestamp(self):
        return self.timestamp

    def __init__(self, gate):
        self.gate = gate

    def __repr__(self):
        return '{' + str(self.id) + ', ' + str(self.timestamp) + '}'
