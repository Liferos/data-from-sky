from api import db


class Cam(db.Model):
    __tablename__ = 'cam'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    url = db.Column(db.String)
    profiles = db.relationship('Profile', backref='cam', lazy=True)

    def get_title(self):
        return self.title

    def get_url(self):
        return self.url

    def __init__(self, name, url):
        self.title = name
        self.url = url

    def __repr__(self):
        return '{' + str(self.id) + ', ' + self.title + ', ' + str(self.latitude) + ', ' + str(self.longitude) + '}'
