from api import db, gate_object_relationship


class Gate(db.Model):
    __tablename__ = 'gate'

    id = db.Column(db.Integer, primary_key=True)
    startX = db.Column(db.Float)
    startY = db.Column(db.Float)
    endX = db.Column(db.Float)
    endY = db.Column(db.Float)
    profile = db.Column(db.Integer, db.ForeignKey('profile.id'))
    object_ = db.relationship('Object', secondary=gate_object_relationship)
    direction = db.Column(db.Integer, db.ForeignKey('direction.id'))
    statistics = db.relationship('Statistic', backref='gate', lazy=True)

    first_count = 0
    second_count = 0
    cars_number = 0

    def get_start_x(self):
        return self.startX

    def get_start_y(self):
        return self.startY

    def get_end_x(self):
        return self.endX

    def get_end_y(self):
        return self.endY

    def __init__(self, start_x, start_y, end_x, end_y, profile, object_, direction):
        self.startX = start_x
        self.startY = start_y
        self.endX = end_x
        self.endY = end_y
        self.profile = profile
        self.object_ = object_
        self.direction = direction

    def __repr__(self):
        return '{' + str(self.id) + ', ' + str(self.startX) + ', ' + str(self.startY) + ', ' \
               + str(self.endX) + ', ' + str(self.endY) + ', ' + str(self.profile_id) + ', ' + str(self.direction_id) \
               + ', ' + str(self.entity_id) + '}'
