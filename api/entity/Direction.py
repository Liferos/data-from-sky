from api import db


class Direction(db.Model):
    __tablename__ = 'direction'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)
    gate = db.relationship('Gate', backref='direction', lazy=True)

    def get_value(self):
        return self.value

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return '{' + str(self.id) + ', ' + self.value + '}'
