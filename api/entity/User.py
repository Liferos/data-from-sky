from api import db


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String)
    password = db.Column(db.String)
    profiles = db.relationship('Profile', backref='user', lazy=True)

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '{' + str(self.id) + ', ' + self.username + ', ' + self.password + '}'
