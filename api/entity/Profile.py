from api import db


class Profile(db.Model):
    __tablename__ = 'profile'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    cam = db.Column(db.Integer, db.ForeignKey('cam.id'))

    def get_id(self):
        return self.id

    def __init__(self, user, cam):
        self.user_id = user
        self.cam_id = cam

    def __repr__(self):
        return '{' + str(self.id) + ', ' + str(self.user_id) + ', ' + str(self.cam_id) + '}'
