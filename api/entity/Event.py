from api import db


class Event(db.Model):
    __tablename__ = 'event'

    id = db.Column(db.Integer, primary_key=True)
    statistic_id = db.Column(db.Integer, db.ForeignKey('statistic.id'))
    object_id = db.Column(db.Integer, db.ForeignKey('object.id'))
    number = db.Column(db.Integer)

    def get_object(self):
        return self.object_id

    def get_statistic(self):
        return self.statistic_id

    def get_number(self):
        return self.number

    def __init__(self, object_, statistic, number):
        self.object_id = object_
        self.statistic_id = statistic
        self.number = number

    def __repr__(self):
        return '{' + str(self.id) + ', ' + str(self.object_id) + ', ' + \
               str(self.statistic) + ', ' + str(self.number) + '}'
