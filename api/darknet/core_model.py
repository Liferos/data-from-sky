from core_configs import *
from core_properties import *


class Model:

    model_structure = None
    model_weights = None
    model_names = None
    ann = None
    meta = None

    def __init__(self, model_structure, model_weights, model_names):
        self.model_structure = model_structure
        self.model_weights = model_weights
        self.model_names = model_names

    def get_model(self):
        if self.is_ann_loaded() or self.is_meta_loaded():
            return self.ann
        else:
            raise Exception('Model was not loaded')

    def get_meta(self):
        if self.is_meta_loaded():
            return self.meta
        else:
            raise Exception('Meta was not loaded')

    def prepare_model(self):
        self.load_model()
        self.load_meta()
        if self.is_meta_loaded() and self.is_ann_loaded():
            print('Model loaded successfully')
        else:
            raise Exception('Something goes wrong, when the model was loading')

    def load_model(self):
        if self.model_structure is not None and self.model_weights is not None:
            self.ann = load_net(self.model_structure, self.model_weights, 0)
        else:
            raise Exception('Model properties haven`t been loaded')

    def load_meta(self):
        if self.model_names is not None:
            self.meta = load_meta(self.model_names)
        else:
            raise Exception('Model names haven`t been loaded')

    def is_meta_loaded(self):
        return self.meta is not None

    def is_ann_loaded(self):
        return self.ann is not None




