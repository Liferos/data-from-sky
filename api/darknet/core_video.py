import cv2


class Video:

    path = None
    name = None
    video = None
    frames = []

    def __init__(self, path):
        self.path = path

    def get_video_width(self):
        return self.video.get(propId=3)

    def get_video_height(self):
        return self.video.get(propId=4)

    def get_video_fps_rate(self):
        return self.video.get(propId=5)

    def get_video_fps_len(self):
        return self.video.get(propId=7)

    def process_frames(self):
        self.video = cv2.VideoCapture(self.path)
        if self.video.isOpened():
            self.video.set(3, 640)
            self.video.set(4, 480)
            for i in range(0, int(self.video.get(propId=7))):
                self.frames.append(self.video.read())
        return self.frames

    def close_video(self):
        if self.video.isOpened():
            self.video.release()

    def get_frame(self, number):
        return self.frames.__getitem__(number)
