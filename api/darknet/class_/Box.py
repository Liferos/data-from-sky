from ctypes import Structure, c_float


class BOX(Structure):

    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]
