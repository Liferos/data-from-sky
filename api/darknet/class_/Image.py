from ctypes import Structure, c_int, c_float, POINTER


class IMAGE(Structure):

    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]
