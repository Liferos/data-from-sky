from ctypes import Structure, c_int, c_float, POINTER
from api.darknet.class_.Box import BOX


class DETECTION(Structure):

    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]
