from ctypes import Structure, c_int, c_char_p, POINTER


class METADATA(Structure):

    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]
