from api.repository import StatisticRepository as stat_repo
import datetime


def save_stat(statistic):
    statistic.timestamp = datetime.datetime.utcnow()
    return stat_repo.save_stat(statistic)


def get_stat(stat_id):
    return stat_repo.get_stat(stat_id)


def get_stat_list():
    return stat_repo.get_stat_list()


def delete_stat(stat_id):
    return stat_repo.delete_stat(stat_id)
