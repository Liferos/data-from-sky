from api.repository import ObjectRepository as object_repo


def save_object(object_):
    return object_repo.save_object(object_)


def get_object(object_id):
    return object_repo.get_object(object_id)


def get_object_list():
    return object_repo.get_object_list()


def delete_object(object_id):
    return object_repo.delete_object(object_id)
