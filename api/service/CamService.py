from api.repository import CamRepository as cam_repo


def save_cam(cam):
    cam_repo.save_cam(cam)


def delete_cam(cam_id):
    cam_repo.delete_cam(cam_id)


def get_cam(cam_id):
    return cam_repo.get_cam(cam_id)


def get_cam_list():
    return cam_repo.get_cam_list()
