from flask import Flask
from flask_sqlalchemy import SQLAlchemy

lib = None
predict = None
set_gpu = None
make_image = None
get_network_boxes = None
make_network_boxes = None
free_detections = None
free_ptrs = None
network_predict = None
reset_rnn = None
load_net = None
do_nms_obj = None
do_nms_sort = None
free_image = None
letterbox_image = None
load_meta = None
load_image = None
rgbgr_image = None
predict_image = None


app = Flask(__name__)
app.config.from_pyfile('config.py')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

gate_object_relationship = db.Table('gate_object_relationship',
                                    db.Column('gate_id', db.Integer, db.ForeignKey('gate.id')),
                                    db.Column('object_id', db.Integer, db.ForeignKey('object_id'))
                                    )
