from api import app
from api.service import StatisticService as stat_serv

from json import JSONEncoder as jsonEnc


@app.route('/statistic', methods=['GET'])
def get_stat_list():
    return jsonEnc.encode(jsonEnc(), str(stat_serv.get_stat_list()))


@app.route('/statistic/<int:stat_id>')
def get_stat(stat_id):
    return stat_serv.get_stat(stat_id)
