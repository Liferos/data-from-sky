from api import app
from api.entity.Cam import Cam
from api.service import CamService as cam_serv
from flask import request, Response

from json import JSONDecoder as jsonDec
from json import JSONEncoder as jsonEnc


@app.route('/cam', methods=['POST'])
def save_cam():
    data = jsonDec.decode(jsonDec(), request.data)
    return Response(cam_serv.save_cam(Cam(data['name'], float(data['latitude']), float(data['longitude']))))


@app.route('/cam/<int:cam_id>', methods=['GET'])
def get_cam(cam_id):
    return cam_serv.get_cam(cam_id)


@app.route('/cam', methods=['GET'])
def get_cam_list():
    return jsonEnc.encode(jsonEnc(), str(cam_serv.get_cam_list()))


@app.route('/cam/<int:cam_id>', methods=['DELETE'])
def delete_cam(cam_id):
    return cam_serv.delete_cam(cam_id)
