from api import app
from flask import render_template
from json import JSONEncoder as jsonEnc


@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')
