from api import app
from api.controller import StatisticController, CamController, ProfileController
from api.controller import GateController, UserController, ViewController
from api.darknet import core_configs, core_methods
from api.handler.ModelHandler import ModelHandler
from api.service import StatisticService
from api.repository import GateRepository
from api.repository import ObjectRepository
from api.entity.Statistic import Statistic
from api.entity.Event import Event


@app.route('/test', methods=['GET'])
def test():
    gate = GateRepository.get_gate(gate_id=1)
    object_ = ObjectRepository.get_object(object_id=0)
    stat = Statistic(gate)
    stat.events.append(Event(object_, 10, stat))
    StatisticService.save_stat(stat)


if __name__ == '__main__':
    app.run()
