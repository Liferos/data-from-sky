from api.darknet.core_model import Model
from api.darknet.core_properties import model_names, model_weights, model_structure
from api.darknet.core_video import Video
from api.darknet.core_methods import array_to_image, detect
from api.entity.Gate import Gate
import cv2


class ModelHandler:

    model = None
    gap_motion = 5
    gap_start = 5
    gap_end = 5

    # TODO need to remove next in the future
    gates = []
    gate1_x1 = None
    gate1_y1 = None
    gate1_x2 = None
    gate1_y2 = None

    gate2_x1 = None
    gate2_y1 = None
    gate2_x2 = None
    gate2_y2 = None

    gate3_x1 = None
    gate3_y1 = None
    gate3_x2 = None
    gate3_y2 = None
    gate7_y2 = None

    gate1 = None
    gate2 = None
    gate3 = None

    def __init__(self):
        print('Model handler created')
        self.model = Model(model_structure, model_weights, model_names)
        self.model.prepare_model()
        self.model = self.model.get_model()

    def test_init_gates(self):

        self.gate1_x1 = 120
        self.gate1_y1 = 350
        self.gate1_x2 = 250
        self.gate1_y2 = 350

        self.gate2_x1 = 720
        self.gate2_y1 = 250
        self.gate2_x2 = 800
        self.gate2_y2 = 250

        self.gate3_x1 = 400
        self.gate3_y1 = 250
        self.gate3_x2 = 450
        self.gate3_y2 = 250

        self.gate1 = Gate(self.gate1_x1, self.gate1_y1, self.gate1_x2, self.gate1_y2, 0, 0, "34")
        self.gates.append(self.gate1)
        self.gate2 = Gate(self.gate2_x1, self.gate2_y1, self.gate2_x2, self.gate2_y2, 0, 0, "43")
        self.gates.append(self.gate2)
        self.gate3 = Gate(self.gate3_x1, self.gate3_y1, self.gate3_x2, self.gate3_y2, 0, 0, "34")
        self.gates.append(self.gate3)

    def process_video(self, path_to_video):
        self.test_init_gates()
        video = Video(path_to_video)
        video.process_frames()
        detected_frames = []
        for i in range(video.get_video_fps_len()):
            detected_data = detect(self.model.get_model(), self.model.get_meta(), array_to_image(video.get_frame(i)))

            for j in range(self.gates.__len__()):
                x = int(detected_data[j][2][0])
                y = int(detected_data[j][2][1])
                w_obj = int(detected_data[j][2][2])
                h_obj = int(detected_data[j][2][3])
                cv2.rectangle(video.get_frame(i),
                              (x - w_obj / 2, y - h_obj / 2),
                              (x + w_obj / 2, y + h_obj / 2),
                              (255, 0, 0), 3)
                # TODO need to refactor this and replace in the another method
                for k in range(len(detected_data)):
                    # gate 1 34
                    if self.gates[k].direction == "34":

                        if (self.gates[k].y1 - self.gap_motion) > y > (self.gates[k].y1 - self.gap_start) and \
                                self.gates[k].x1 < x < self.gates[k].x2:
                            self.gates[k].first_count = self.gates[k].first_count + 1
                        # print("34 first_count #" + str(self.gate[i].first_count))

                        if self.gates[k].first_count > 1 and \
                                (self.gates[k].y1 + self.gap_motion) < y < (self.gates[k].y1 + self.gap_start) \
                                and self.gates[k].x1 < x < self.gates[k].x2:
                            self.gates[k].second_count = self.gates[k].second_count + 1
                        # print("34 second_count #" + str(self.gate[i].second_count))

                        if self.gates[k].first_count > 0 and self.gates[k].second_count > 0:
                            self.gates[k].cars_number = self.gates[k].cars_number + 1
                            self.gates[k].first_count = 0
                            self.gates[k].second_count = 0
                            print("34 #" + str(self.gates[k].cars_number))

                    if self.gates[k].direction == "43":

                        if (self.gates[k].y1 + self.gap_motion) < y < (self.gates[k].y1 + self.gap_start) \
                                and self.gates[k].x1 < x < self.gates[k].x2:
                            self.gates[k].first_count = self.gates[k].first_count + 1
                        # print("43 first_count #" + str(self.gate[i].second_count))

                        if self.gates[k].first_count > 1 and (self.gates[k].y1 - self.gap_motion) > y > \
                                (self.gates[k].y1 - self.gap_start) \
                                and self.gates[k].x1 < x < self.gates[k].x2:
                            self.gates[k].second_count = self.gates[k].second_count + 1
                        print("43 first_count #" + str(self.gates[k].second_count))

                        if self.gates[k].first_count > 0 and self.gates[k].second_count > 0:
                            self.gates[k].cars_number = self.gates[k].cars_number + 1
                            self.gates[k].first_count = 0
                            self.gates[k].second_count = 0

                    if self.gates[k].direction == "12":

                        if (self.gates[k].x1 - self.gap_motion) > x > (self.gates[k].x1 - self.gap_start) and \
                                self.gates[k].y1 < y < self.gates[k].y2:
                            self.gates[k].first_count = self.gates[k].first_count + 1
                            print("12 first_count #" + str(self.gates[k].first_count))

                        if self.gates[k].first_count > 0 and (self.gates[k].x1 + self.gap_motion) < x < (
                                self.gates[k].x1 + self.gap_start) and self.gates[k].y1 < y < self.gates[k].y2:
                            self.gates[k].second_count = self.gates[k].second_count + 1
                            print("12 second_count #" + str(self.gates[k].second_count))

                        if self.gates[k].first_count > 0 and self.gates[k].second_count > 0:
                            self.gates[k].cars_number = self.gates[k].cars_number + 1
                            self.gates[k].first_count = 0
                            self.gates[k].second_count = 0

                    if self.gates[k].direction == "21":

                        if (self.gates[k].x1 + self.gap_motion) < x < (self.gates[k].x1 + self.gap_start) and \
                                self.gates[k].y1 < y < self.gates[k].y2:
                            self.gates[k].first_count = self.gates[k].first_count + 1
                            print("21 first_count #" + str(self.gates[k].first_count))

                        if self.gates[k].first_count > 1 and (self.gates[k].x1 - self.gap_motion) > x > (
                                self.gates[k].x1 - self.gap_start) and self.gates[k].y1 < y < self.gates[k].y2:
                            self.gates[k].second_count = self.gates[k].second_count + 1
                            print("21 second_count #" + str(self.gates[k].second_count))

                        if self.gates[k].first_count > 0 and self.gates[k].second_count > 0:
                            self.gates[k].cars_number = self.gates[k].cars_number + 1
                            self.gates[k].first_count = 0
                            self.gates[k].second_count = 0

            for j in range(self.gates.__len__()):
                cv2.line(video.get_frame(i), (int(self.gates[j].x1), int(self.gates[j].y1)),
                         (int(self.gates[j].x2), int(self.gates[j].y2)), (0, 0, 255), 2)
                cv2.putText(video.get_frame(i), "gate: {}".format(str(self.gates[j].cars_number)),
                            (int(self.gates[j].x1), int(self.gates[j].y1) + 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)

            detected_frames.append(cv2.imencode('.jpg', video.get_frame(i)).tobytes())
        return detected_frames
