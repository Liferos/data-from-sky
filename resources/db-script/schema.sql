
CREATE TABLE IF NOT EXISTS statistic
(
  id BIGSERIAL NOT NULL,
  gate_id bigserial NOT NULL,
  "timestamp" timestamp without time zone NOT NULL,
  CONSTRAINT statistic_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS  event
(
  id BIGSERIAL NOT NULL,
  statistic_id BIGSERIAL NOT NULL,
  object_id BIGSERIAL NOT NULL,
  number INTEGER NOT NULL DEFAULT 0,
  CONSTRAINT event_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS gate
(
  id BIGSERIAL NOT NULL,
  startX FLOAT NOT NULL,
  startY FLOAT NOT NULL,
  endX FLOAT NOT NULL,
  endY FLOAT NOT NULL,
  direction_id BIGSERIAL NOT NULL,
  profile_id BIGSERIAL NOT NULL,
  CONSTRAINT gate_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS object
(
  id BIGSERIAL NOT NULL,
  value VARCHAR(250),
  CONSTRAINT object_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS gate_object_relationship
(
  id BIGSERIAL NOT NULL,
  gate_id BIGSERIAL NOT NULL,
  object_id BIGSERIAL NOT NULL,
  CONSTRAINT gate_object_relationship_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS direction
(
  id BIGSERIAL NOT NULL,
  value VARCHAR(250),
  CONSTRAINT direction_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS profile
(
  id BIGSERIAL NOT NULL,
  user_id BIGSERIAL NOT NULL,
  cam_id BIGSERIAL NOT NULL,
  CONSTRAINT profile_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users
(
  id BIGSERIAL NOT NULL,
  username VARCHAR(255),
  password VARCHAR(255),
  CONSTRAINT users_pk_id PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cam
(
  id BIGSERIAL NOT NULL,
  title VARCHAR(255),
  url VARCHAR(512),
  CONSTRAINT cam_pk_id PRIMARY KEY (id)
);

ALTER TABLE public.statistic
ADD CONSTRAINT statistic_gate_fk
FOREIGN KEY (gate_id) REFERENCES gate (id);

ALTER TABLE public.event
ADD CONSTRAINT event_statistic_fk
FOREIGN KEY (statistic_id) REFERENCES statistic (id);

ALTER TABLE public.event
ADD CONSTRAINT event_object_fk
FOREIGN KEY (object_id) REFERENCES object (id);

ALTER TABLE public.gate
ADD CONSTRAINT gate_direction_fk
FOREIGN KEY (direction_id) REFERENCES direction (id);

ALTER TABLE public.gate_object_relationship
ADD CONSTRAINT relationship_gate_fk
FOREIGN KEY (gate_id) REFERENCES gate (id);

ALTER TABLE public.gate_object_relationship
ADD CONSTRAINT relationship_object_fk
FOREIGN KEY (object_id) REFERENCES object (id);

ALTER TABLE public.gate
ADD CONSTRAINT gate_profile_fk
FOREIGN KEY (profile_id) REFERENCES profile (id);

ALTER TABLE public.profile
ADD CONSTRAINT profile_user_fk
FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE public.profile
ADD CONSTRAINT profile_cam_fk
FOREIGN KEY (cam_id) REFERENCES cam (id);
