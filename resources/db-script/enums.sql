
DELETE FROM object WHERE id in ('0', '1', '2', '3');
INSERT INTO object(id, value) VALUES
('0', 'PEDESTRIAN'),
('1', 'MOTORCYCLIST'),
('2', 'CAR'),
('3', 'TRUCK');

DELETE FROM direction WHERE id in ('0', '1', '2', '3');
INSERT INTO direction(id, value) VALUES
('0', 'UP'),
('1', 'DOWN'),
('2', 'LEFT'),
('3', 'RIGHT');